#!/usr/bin/bash

runname="run_1115_default"

# Generate clusters for MVA training
# Only 300k events here. Move up to 3M if needed
python runParallel.py --outDir $runname/production --nEvt 300000 --production --SF $runname/sampling/SF.json --corrections $runname/upstream/corr_params_1d.json

# Then train the MVA
python training.py CaloClusters -i $runname/production/ --json $runname/upstream/corr_params_1d.json -o $runname/training.json

# Run clustering algs to compute resolutions
python runParallel.py --outDir $runname/clusters --nEvt 10000 --energies 1000 5000 10000 20000 30000 50000 100000 --clusters --SF $runname/sampling/SF.json --corrections $runname/upstream/corr_params_1d.json

# Compute resolutions and responses
python compute_resolutions.py --inputDir $runname/clusters --outFile $runname/results.csv --clusters CaloClusters CorrectedCaloClusters CaloTopoClusters CorrectedCaloTopoClusters --MVAcalib $runname/training.json

# And make plots
python plot_resolutions.py --outDir $runname --doFits plot $runname/results.csv --all
python plot_resolutions.py --outDir $runname --doFits compare clusters CaloClusters CorrectedCaloClusters CaloTopoClusters CorrectedCaloTopoClusters CalibratedCaloClusters $runname/results.csv --all
