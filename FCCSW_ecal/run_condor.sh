#!/bin/bash

cd /afs/cern.ch/work/l/lit/fcc/calo_sim/work_1011/
source env.sh

cd /afs/cern.ch/work/l/lit/fcc/calo_sim/work_1011/FCCAnalyses/
source setup.sh

cd /afs/cern.ch/work/l/lit/fcc/calo_sim/work_1011/LAr_scripts/FCCSW_ecal/
. run_1115_default.sh

